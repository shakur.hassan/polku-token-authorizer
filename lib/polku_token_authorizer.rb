# frozen_string_literal: true

require_relative "polku_token_authorizer/version"

module PolkuTokenAuthorizer
  autoload :AppService, "polku_token_authorizer/app_service"
  autoload :Error, "polku_token_authorizer/error"
  autoload :GenerateToken, "polku_token_authorizer/generate_token"
  autoload :AuthorizeRequest, "polku_token_authorizer/authorize_request"
end
