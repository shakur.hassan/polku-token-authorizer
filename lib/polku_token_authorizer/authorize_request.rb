require 'jwt'
require 'openssl'

module PolkuTokenAuthorizer
  class AuthorizeRequest < AppService
    attr_reader :token

    def initialize(jwt_public_key, token)
      @jwt_public_key = jwt_public_key
      @token = token
    end

    def call
      # Get the token from the headers
      auth_token = @token.split(' ').last
      # Decode the token to get user data (payload)
      decode(auth_token)
    rescue StandardError => e
      "Error decoding token"
    end

    def decode(token)
      # Decodes the token to get user data (payload)
      body = JWT.decode token, OpenSSL::PKey::RSA.new(
        @jwt_public_key),true, {algorithm: 'RS256' }

      body.reduce({}, :update)

      # Raise custom error to be handled by custom handler
    rescue JWT::ExpiredSignature, JWT::VerificationError => e
      raise ExceptionHandler::ExpiredSignature, e.message
    rescue JWT::DecodeError, JWT::VerificationError => e
      raise ExceptionHandler::DecodeError, e.message
    rescue StandardError => e
      "Error decoding token"
    end
  end
end