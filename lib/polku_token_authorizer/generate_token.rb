require 'jwt'
require 'openssl'

module PolkuTokenAuthorizer
  class GenerateToken < AppService
    attr_reader :client_id, :user

    def initialize(jwt_private_key, client_id, user=nil)
      @jwt_private_key = jwt_private_key
      @client_id = client_id
      @user = user || {}
    end

    def call
      payload = { client_id: @client_id, user: @user.as_json }
      rsa_private = OpenSSL::PKey::RSA.new(@jwt_private_key)
      JWT.encode(payload, rsa_private, 'RS256')
    rescue StandardError => e
      "Error generating token"
    end
  end
end