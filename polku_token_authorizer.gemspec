# frozen_string_literal: true

require_relative "lib/polku_token_authorizer/version"

Gem::Specification.new do |spec|
  spec.name = "polku_token_authorizer"
  spec.version = PolkuTokenAuthorizer::VERSION
  spec.authors = ["Shakur"]
  spec.email = ["shakur.hassan@autolle.com"]

  spec.summary = "Authorization of Polku API tokens."
  spec.description = "The Gem is used to authorize Polku API tokens. The Gem Decodes and verifies the token from the request. The Gem can also encode, create and decode tokens for the client application."
  spec.homepage = "https://gitlab.com/comille-alusta/polku-token-authorizer"
  spec.required_ruby_version = ">= 2.6.0"

  #spec.metadata["allowed_push_host"] = "TODO: Set to your gem server 'https://example.com'"

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = "https://gitlab.com/comille-alusta/polku-token-authorizer"
  spec.metadata["changelog_uri"] = "https://gitlab.com/comille-alusta/polku-token-authorizer/-/blob/main/CHANGELOG.md"

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0").reject do |f|
      (f == __FILE__) || f.match(%r{\A(?:(?:test|spec|features)/|\.(?:git|travis|circleci)|appveyor)})
    end
  end
  spec.bindir = "exe"
  spec.executables = spec.files.grep(%r{\Aexe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  # Uncomment to register a new dependency of your gem
  # spec.add_dependency "example-gem", "~> 1.0"

  # For more information and examples about making a new gem, checkout our
  # guide at: https://bundler.io/guides/creating_gem.html
  spec.add_dependency 'jwt', '>= 1.0.0', '< 3.0.0'
  spec.add_dependency 'openssl', '~> 2.2.0'
end
